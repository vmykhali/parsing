<?php
$doc = new DOMDocument;
$content = file_get_contents('https://www.liga.net/fin/crypto/rss.xml');
$items = new SimpleXmlElement( $content );
foreach ( $items->channel->item as $key => $item ) {
    echo '<div class="news__item">';
    echo "<h4>".$item->title."</h4>";
    echo '<img src="'.$item->enclosure['url'].'">';
    echo '<p>'.$item->description.'</p>';
    echo '<br><br><a href="'.$item->link. '">View more</a><br>';
    echo '<br></div>';
    echo "<link rel='stylesheet' href='style.css'>";
}